#!/usr/bin/env python

import getopt
import json
import pika
import sys


def usage():
  print('Usage: %s OPTIONS' % sys.argv[0])
  print('\nOPTIONS:')
  print(' [-u] (--user)      user with permissions on the queue')
  print(' [-p] (--password)  password for the user')
  print(' -q (--queue)       queue to monitor')
  print(' [-t] (--target)    IP RabbitMQ is bound to')
  print(' [-v] (--vhost)     defaults to "/"')
  print(' [-h] (--help)      print this message')


def callback(ch, method, properties, body):
  print("%s\n" % json.loads(body))
   

def main():
  try:
    opts, args = getopt.getopt(sys.argv[1:], "u:p:t:q:v:h", ["user=", "password=", "target=", "queue=", "vhost", "help"])
  except getopt.GetoptError as err:
    print(str(err))
    usage()
    return 1
  for opt, arg in opts:
    if opt in ("-u", "--user"):
      user = arg
    elif opt in ("-p", "--password"):
      password = arg
    elif opt in ("-q", "--queue"):
      queue_name = arg
    elif opt in ("-t", "--target"):
      target = arg
    elif opt in ("-v", "--vhost"):
      vhost = arg
    elif opt in ("-h", "--help"):
      usage()
      return 0
    else:
      assert False, '<*> unhandled option'
      return 1

  # set defaults if unspecified
  try:
    user
  except:
    user = 'guest'
    print("> using 'guest' as the username")

  try:
    password
  except:
    print("> using 'guest' as the password")
    password = 'guest'

  try:
    target
  except:
    target = 'localhost'
    print("> connecting to 'localhost'")

  try:
    vhost
  except:
    vhost = '/'
    print("> using '/' vhost")

  try:
    queue_name
  except:
    queue_name= 'conductor'
    print("> consuming from 'conductor' queue")

  # connect to RabbitMQ
  credentials = pika.PlainCredentials(user, password)
  connection = pika.BlockingConnection(pika.ConnectionParameters(
            target,
            5672,
            vhost,
            credentials))
  channel = connection.channel()

  try:
    channel.queue_declare(queue=queue_name, passive=True)
  except:
    print("<!> queue does not exist: %s" % queue_name)
    return 1
        
  channel.basic_consume(queue_name,
                        callback,
                        auto_ack=False,
                        exclusive=False,
                        consumer_tag='rmq_mon')
    
  try:
    channel.start_consuming()
  except KeyboardInterrupt:
    channel.stop_consuming()

  connection.close()

if __name__ == "__main__":
  rc = main()
  exit(rc)

