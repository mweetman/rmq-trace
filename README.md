These scripts consume messages from RabbitMQ, the instructions assume you are running OpenStack, please adjust as required.

## Instructions

Find the RabbitMQ IP address:
```
# netstat -tnlp | grep :5672
tcp        0      0 172.24.1.1:5672         0.0.0.0:*               LISTEN      12394/beam.smp
```

Add a user to consume the messages:
```
# docker exec -t rabbitmq-bundle-docker-0 rabbitmqctl add_user tracer Password1
# docker exec -t rabbitmq-bundle-docker-0 rabbitmqctl set_permissions tracer ".*" ".*" ".*"
```
```
# podman exec -t rabbitmq-bundle-podman-0 rabbitmqctl add_user tracer Password1
# podman exec -t rabbitmq-bundle-podman-0 rabbitmqctl set_permissions tracer ".*" ".*" ".*"
```

### For tracing all messages

Enable tracing:
```
# docker exec -t rabbitmq-bundle-docker-0 rabbitmqctl trace_on
```
```
# podman exec -t rabbitmq-bundle-podman-0 rabbitmqctl trace_on
```

Start consuming messages from the trace exchange:
```
# rmq_trace.py -u tracer -p Password1 -t 172.24.1.1 > /tmp/rabbit.trace
```

Do some stuff to generate the messages you're interested in.

When done, turn tracing off:
```
# docker exec -t rabbitmq-bundle-docker-0 rabbitmqctl trace_off
```
```
# podman exec -t rabbitmq-bundle-podman-0 rabbitmqctl trace_off
```

### For consuming a specific queue

Start consuming messages from the target queue:
```
# rmq_mon.py -u tracer -p Password1 -t 172.24.1.1 -v "/" -q conductor
```

